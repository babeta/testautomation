package smartest.automation.academy.tests.execution;

import org.testng.annotations.*;

public class TestExecutionClass {

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("Before suite execution");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("Before test execution");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("Before class execution");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Before method execution");
    }

    @Test
    public void login() {
        System.out.println("Login test execution");
    }

    @Test
    public void logout() {
        System.out.println("Logout test execution");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("After method execution");
    }

    @AfterClass
    public void afterClass() {
        System.out.println("After class execution");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("After test execution");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("After suite execution");
    }
}
