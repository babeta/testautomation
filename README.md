# Test automation academy #

Project for test automation academy including Selenium WebDriver

## Návod ke stažení ##

1. Zklonování projektu skrze git: git clone https://hornyja4@bitbucket.org/smartestautomation/testautomation.git
2. Vytvoření forku této repository do vlastního repositáře a následné stažení skrze git.
3. Stažení projektu z Downloads

## Návod na spuštění projektu ##

Jedná se o maven projekt, který se otevírá souborem pom.xml.
V tomto souboru jsou nastaveny závislosti na všechny používané a požadované nástroje/knihovny jako je Selenium, TestNG, Log4j2 apod.

V IntelliJ Idea i dalších IDE otevřít projekt skrze:

File -> Open -> najít v průzkumníku projekt a otevřít soubor pom.xml